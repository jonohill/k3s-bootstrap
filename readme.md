# K3s Bootstrap

A python script/docker container which bootstraps a k3s master node.
My use case is for bootstrapping a Hypriot-based k3s cluster.

Currently the script:
- Installs Helm Tiller
- Installs and configures Flux

The script uses data from a json file to populate config and secrets, the default location is `/bootstrap.json`.

```
{
    "flux": {
        "options": {}
    },
    "ssh_key": ""
}
```

`flux.options` will be passed to the Flux Helm chart. You'll want to specify at least `git.url`.
`ssh_key` is an OpenSSH private key which Flux will use to authenticate to the Git server.
