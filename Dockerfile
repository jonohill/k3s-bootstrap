ARG K3S_VERSION=latest
FROM rancher/k3s:${K3S_VERSION} AS k3s

FROM python:3.7-alpine

ENV KUBECONFIG=/data/k3s/kubeconfig.yaml
ENV DATA_FILE=/data/bootstrap.json

RUN apk add --no-cache \
    bash \
    curl \
    openssl

COPY --from=k3s /bin/kubectl /usr/local/bin/kubectl

RUN curl -L https://git.io/get_helm.sh | bash

WORKDIR /usr/src/bootstrap

COPY . .

CMD [ "python3", "bootstrap.py" ]
