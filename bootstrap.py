#!/usr/bin/env python3

from subprocess import run, DEVNULL
import os
from os import path
from time import sleep
import math
import json
import shlex
import logging

log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)

KUBECONFIG = os.environ.get('KUBECONFIG', '/data/k3s/kubeconfig.yaml')
DATA_FILE = os.environ.get('DATA_FILE', '/data/bootstrap.json')

def sh(*args, **kwargs):
    defaults = {
        'check': True,
        'shell': True
    }
    defaults.update(kwargs)
    return run(*args, **defaults)

def poll_until_true(func, max_time=0):
    wait_time = 0
    while not func():
        sleep(5)
        wait_time += 5
        if wait_time % 60 == 0:
            log.warn('Still waiting after {} seconds...'.format(wait_time))
        if max_time > 0 and wait_time >= max_time:
            return False
    return True

def sh_until_success(cmd, max_time=0):
    return poll_until_true(lambda: sh(cmd, check=False, stdout=DEVNULL, stderr=DEVNULL).returncode == 0, max_time)

data = {}
with open(DATA_FILE, 'r') as f:
    data = json.load(f)

log.info('Waiting for the kubeconfig to appear...')
poll_until_true(lambda: path.isfile(KUBECONFIG))
os.environ['KUBECONFIG'] = KUBECONFIG

log.info('Waiting for cluster to be healthy...')
sh_until_success('kubectl cluster-info')

log.info('Installing Helm Tiller...')
if sh_until_success('helm version', 1):
    sh('helm init --client-only')
else:
    # TODO swap out the image once official ones are multiarch
    sh('kubectl -n kube-system create sa tiller')
    sh('kubectl create clusterrolebinding tiller-cluster-rule \
        --clusterrole=cluster-admin \
        --serviceaccount=kube-system:tiller')
    sh('helm init --skip-refresh --upgrade --service-account tiller --tiller-image=jessestuart/tiller --history-max 10')
    log.info('Waiting for Tiller to be ready...')
    sh_until_success('helm version')

log.info('Installing Flux...')
sh('helm repo add fluxcd https://fluxcd.github.io/flux')
sh('helm repo update')
sh('kubectl apply -f https://raw.githubusercontent.com/fluxcd/flux/master/deploy-helm/flux-helm-release-crd.yaml')

flux_opts = {
    'helmOperator.create': 'true',
    'helmOperator.createCRD': 'false',
    'git.secretName': 'flux-git-deploy'
}
flux_opts.update(data['flux']['options'])

opts_str = ' '.join(['--set {k}={v}'.format(k=k,v=v) for k, v in flux_opts.items()])

sh('helm install --name flux {} --namespace flux fluxcd/flux'.format(opts_str))

log.info('Creating Git secret for Flux...')
with open('flux_key', 'w') as f:
    f.write(data['flux']['ssh_key'])
sh('kubectl create secret generic {} --namespace flux --from-file=identity=flux_key'.format(flux_opts["git.secretName"]))
